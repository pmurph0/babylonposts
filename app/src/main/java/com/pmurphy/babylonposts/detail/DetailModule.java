package com.pmurphy.babylonposts.detail;

import com.pmurphy.babylonposts.data.model.Post;
import com.pmurphy.babylonposts.data.source.DataSource;
import com.pmurphy.babylonposts.util.AppSchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
class DetailModule {

    private final DetailContract.View view;
    private final Post post;

    DetailModule(DetailContract.View view, Post post) {
        this.view = view;
        this.post = post;
    }

    @Provides
    DetailContract.View provideView() {
        return view;
    }

    @Provides
    DetailContract.Presenter providePresenter(DataSource dataSource, DetailContract.View view, Post post,
                                              AppSchedulerProvider appSchedulerProvider) {
        return new DetailPresenter(dataSource, view, post, appSchedulerProvider);
    }

    @Provides
    Post providePost() {
        return post;
    }

}
