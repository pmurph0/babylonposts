package com.pmurphy.babylonposts.detail;

import android.util.Log;

import com.pmurphy.babylonposts.data.model.Comment;
import com.pmurphy.babylonposts.data.model.Post;
import com.pmurphy.babylonposts.data.model.User;
import com.pmurphy.babylonposts.data.source.DataSource;
import com.pmurphy.babylonposts.util.SchedulerProvider;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Presenter for Post details
 */

class DetailPresenter implements DetailContract.Presenter {

    private static final String TAG = DetailPresenter.class.getSimpleName();

    private final DataSource dataSource;
    private final DetailContract.View view;
    private final Post post;
    private final SchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;

    DetailPresenter(DataSource dataSource, DetailContract.View view, Post post, SchedulerProvider schedulerProvider) {
        this.dataSource = dataSource;
        this.view = view;
        this.post = post;
        this.schedulerProvider = schedulerProvider;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        loadPostDetails();
    }

    @Override
    public void unsubscribe() {
        compositeDisposable.clear();
    }

    private Observable<List<Comment>> getComments() {
        return dataSource.getComments()
                .flatMap(Observable::fromIterable)
                .filter(comment -> post.getId().equals(comment.getPostId()))
                .toList()
                .toObservable();
    }

    private Observable<User> getUser() {
        return dataSource.getUsers()
                .flatMap(Observable::fromIterable)
                .filter(user -> post.getUserId().equals(user.getId()))
                .firstElement()
                .toObservable();

    }

    @Override
    public void loadPostDetails() {
        Disposable subscription = Observable.zip(Observable.just(post), getUser(), getComments(),
                PostDetailData::new)
                .observeOn(schedulerProvider.main())
                .subscribe(postDetailData -> {
                    view.showPostTitle(postDetailData.post.getTitle());
                    view.showPostBody(postDetailData.post.getBody());
                    view.showUserName(postDetailData.user.getName());
                    view.showUserAvatar(postDetailData.user.getEmail());
                    view.showComments(postDetailData.comments);
                }, throwable -> Log.e(TAG, "Error fetching post data", throwable));

        compositeDisposable.add(subscription);
    }

    private static class PostDetailData {
        Post post;
        User user;
        List<Comment> comments;

        PostDetailData(Post post, User user, List<Comment> comments) {
            this.post = post;
            this.user = user;
            this.comments = comments;
        }
    }

}
