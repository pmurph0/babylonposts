package com.pmurphy.babylonposts.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.pmurphy.babylonposts.BabylonPostsApplication;
import com.pmurphy.babylonposts.R;
import com.pmurphy.babylonposts.data.model.Comment;
import com.pmurphy.babylonposts.data.model.Post;
import com.pmurphy.babylonposts.util.AvatarUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * Post detail Activity/View
 */

public class DetailActivity extends AppCompatActivity implements DetailContract.View {

    public static final String EXTRA_POST = "post";

    @BindView(R.id.post_detail_title)
    TextView title;
    @BindView(R.id.post_detail_body)
    TextView body;
    @BindView(R.id.post_detail_user_name)
    TextView userName;
    @BindView(R.id.post_detail_comments)
    TextView comments;
    @BindView(R.id.post_detail_toolbar)
    Toolbar toolbar;
    @BindView(R.id.post_detail_user_avatar)
    ImageView userAvatar;

    @BindDimen(R.dimen.post_detail_avatar_size)
    int avatarSize;

    @Inject
    DetailContract.Presenter presenter;
    @Inject
    Picasso picasso;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        DaggerDetailComponent.builder()
                .applicationComponent(((BabylonPostsApplication)getApplication()).getApplicationComponent())
                .detailModule(new DetailModule(this, getIntent().getParcelableExtra(EXTRA_POST)))
                .build()
                .inject(this);

        setUpToolbar();
        presenter.subscribe();
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public static void launch(Context context, Post post) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(EXTRA_POST, post);
        context.startActivity(intent);
    }

    @Override
    public void showPostTitle(String title) {
        this.title.setText(title);
    }

    @Override
    public void showPostBody(String body) {
        this.body.setText(body);
    }

    @Override
    public void showUserName(String name) {
        this.userName.setText(name);
    }

    @Override
    public void showUserAvatar(String email) {
        picasso.load(AvatarUtils.buildAdorableAvatarUrl(email, avatarSize))
                .into(userAvatar);
    }

    @Override
    public void showComments(List<Comment> comments) {
        this.comments.setText(getString(R.string.comments_count, comments.size()));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onPause() {
        if (isFinishing()) {
            presenter.unsubscribe();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }

}
