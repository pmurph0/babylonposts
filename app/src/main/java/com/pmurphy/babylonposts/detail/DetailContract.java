package com.pmurphy.babylonposts.detail;

import com.pmurphy.babylonposts.BasePresenter;
import com.pmurphy.babylonposts.data.model.Comment;

import java.util.List;

interface DetailContract {

    interface View {

        void showPostTitle(String title);

        void showPostBody(String body);

        void showUserName(String name);

        void showUserAvatar(String avatarUrl);

        void showComments(List<Comment> comments);

    }

    interface Presenter extends BasePresenter {
        void loadPostDetails();
    }

}
