package com.pmurphy.babylonposts.detail;

import com.pmurphy.babylonposts.ApplicationComponent;
import com.pmurphy.babylonposts.util.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = DetailModule.class)
interface DetailComponent {
    void inject(DetailActivity detailActivity);
}
