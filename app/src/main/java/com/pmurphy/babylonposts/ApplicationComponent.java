package com.pmurphy.babylonposts;

import com.pmurphy.babylonposts.data.source.DataSource;
import com.pmurphy.babylonposts.util.AppSchedulerProvider;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by PeterMurphy on 20/05/2017.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    DataSource getDataSource();

    Picasso getPicasso();

    AppSchedulerProvider getAppSchedulerProvider();

}
