package com.pmurphy.babylonposts.list;

import com.pmurphy.babylonposts.BasePresenter;
import com.pmurphy.babylonposts.data.model.Post;

import java.util.List;

/**
 * Created by PeterMurphy on 19/05/2017.
 */

public interface ListContract {

    interface Presenter extends BasePresenter {
        void loadPosts();
        void onPostClick(Post post);
    }

    interface View {
        void showPosts(List<Post> posts);
        void showPostDetails(Post post);
    }

}
