package com.pmurphy.babylonposts.list;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pmurphy.babylonposts.R;
import com.pmurphy.babylonposts.data.model.Post;
import com.pmurphy.babylonposts.util.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class PostListAdapter extends RecyclerView.Adapter<PostListAdapter.PostViewHolder> {

    private final List<Post> posts;
    private final ItemClickListener<Post> itemClickListener;

    PostListAdapter(ItemClickListener<Post> itemClickListener) {
        this.itemClickListener = itemClickListener;
        posts = new ArrayList<>();
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PostViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_cell, parent, false));
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        Post post = getPost(position);
        if (post != null) {
            holder.title.setText(post.getTitle());
        }
        holder.post = post;
    }

    @Nullable
    private Post getPost(int position) {
        return position < posts.size() ? posts.get(position) : null;
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    void setPosts(List<Post> posts) {
        this.posts.clear();
        this.posts.addAll(posts);
        notifyItemRangeInserted(0, posts.size());
    }

    class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.post_list_cell_title)
        TextView title;

        Post post;

        PostViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick(post);
        }
    }

}
