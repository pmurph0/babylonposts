package com.pmurphy.babylonposts.list;

import android.util.Log;

import com.pmurphy.babylonposts.data.model.Post;
import com.pmurphy.babylonposts.data.source.DataSource;
import com.pmurphy.babylonposts.util.SchedulerProvider;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Presenter for the Post list screen
 *
 * Created by PeterMurphy on 20/05/2017.
 */
class ListPresenter implements ListContract.Presenter {

    private static final String TAG = ListPresenter.class.getSimpleName();

    private final DataSource dataSource;
    private final ListContract.View view;
    private final SchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;

    ListPresenter(ListContract.View view, DataSource dataSource, SchedulerProvider schedulerProvider) {
        this.view = view;
        this.dataSource = dataSource;
        this.schedulerProvider = schedulerProvider;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void loadPosts() {
        Disposable subscription = dataSource.getPosts()
                .observeOn(schedulerProvider.main())
                .subscribe(posts -> {
                            view.showPosts(posts);
                        },
                        throwable -> {
                            Log.e(TAG, throwable.getLocalizedMessage());
                        });

        compositeDisposable.add(subscription);
    }

    @Override
    public void onPostClick(Post post) {
        view.showPostDetails(post);
    }

    @Override
    public void subscribe() {
        loadPosts();
    }

    @Override
    public void unsubscribe() {
        compositeDisposable.clear();
    }
}
