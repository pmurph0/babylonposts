package com.pmurphy.babylonposts.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.pmurphy.babylonposts.BabylonPostsApplication;
import com.pmurphy.babylonposts.R;
import com.pmurphy.babylonposts.data.model.Post;
import com.pmurphy.babylonposts.detail.DetailActivity;
import com.pmurphy.babylonposts.util.ItemClickListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by PeterMurphy on 19/05/2017.
 */

public class ListActivity extends AppCompatActivity implements ListContract.View, ItemClickListener<Post> {

    @BindView(R.id.list_activity_toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_activity_recyclerview)
    RecyclerView recyclerView;

    @Inject
    ListContract.Presenter presenter;

    private PostListAdapter postListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);

        DaggerListComponent.builder()
                .applicationComponent(((BabylonPostsApplication) getApplication()).getApplicationComponent())
                .listModule(new ListModule(this))
                .build()
                .inject(this);

        setSupportActionBar(toolbar);
        setUpList();
        presenter.subscribe();
    }

    private void setUpList() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        postListAdapter = new PostListAdapter(this);
        recyclerView.setAdapter(postListAdapter);
    }

    @Override
    public void showPosts(List<Post> posts) {
        postListAdapter.setPosts(posts);
    }

    @Override
    public void showPostDetails(Post post) {
        DetailActivity.launch(this, post);
    }

    @Override
    public void onItemClick(Post item) {
        presenter.onPostClick(item);
    }

    @Override
    protected void onPause() {
        if (isFinishing()) {
            presenter.unsubscribe();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }
}
