package com.pmurphy.babylonposts.list;

import com.pmurphy.babylonposts.data.source.DataSource;
import com.pmurphy.babylonposts.util.AppSchedulerProvider;
import com.pmurphy.babylonposts.util.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by PeterMurphy on 20/05/2017.
 */
@Module
class ListModule {

    private final ListContract.View view;

    ListModule(ListContract.View view) {
        this.view = view;
    }

    @Provides
    ListContract.View providesView() {
        return view;
    }

    @Provides
    ListContract.Presenter providePresenter(DataSource dataSource, ListContract.View view, AppSchedulerProvider schedulerProvider) {
        return new ListPresenter(view, dataSource, schedulerProvider);
    }

}
