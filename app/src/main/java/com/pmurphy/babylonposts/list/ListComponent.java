package com.pmurphy.babylonposts.list;

import com.pmurphy.babylonposts.ApplicationComponent;
import com.pmurphy.babylonposts.util.ActivityScope;

import dagger.Component;

/**
 * Created by PeterMurphy on 20/05/2017.
 */
@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = ListModule.class)
interface ListComponent {
    void inject(ListActivity listActivity);
}
