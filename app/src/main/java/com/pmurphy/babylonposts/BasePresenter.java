package com.pmurphy.babylonposts;

/**
 * Created by PeterMurphy on 20/05/2017.
 */

public interface BasePresenter {

    void subscribe();

    void unsubscribe();

}
