package com.pmurphy.babylonposts;

import android.content.Context;

import com.pmurphy.babylonposts.data.api.ApiDataService;
import com.pmurphy.babylonposts.data.provider.DataProvider;
import com.pmurphy.babylonposts.data.source.DataSource;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by PeterMurphy on 20/05/2017.
 */
@Module
class ApplicationModule {

    private static final String API_BASE_URL = "http://jsonplaceholder.typicode.com/";

    private final Context appContext;

    ApplicationModule(Context appContext) {
        this.appContext = appContext;
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Singleton
    @Provides
    ApiDataService providesDataService(Retrofit retrofit) {
        return retrofit.create(ApiDataService.class);
    }

    @Singleton
    @Provides
    DataSource provideDataSource(ApiDataService apiDataService) {
        return new DataProvider(apiDataService);
    }

    @Singleton
    @Provides
    Context provideAppContext() {
        return appContext;
    }

    @Singleton
    @Provides
    Picasso providePicasso(Context context) {
        return Picasso.with(context);
    }

}
