package com.pmurphy.babylonposts.util;

import java.util.Locale;


public class AvatarUtils {

    private static final String ADORABLE_AVATAR_URL = "https://api.adorable.io/avatars/%d/%s.png";

    public static String buildAdorableAvatarUrl(String key, int size) {
        return String.format(Locale.US, ADORABLE_AVATAR_URL, size, key);
    }

}
