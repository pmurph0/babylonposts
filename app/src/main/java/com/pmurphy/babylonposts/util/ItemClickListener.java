package com.pmurphy.babylonposts.util;

/**
 * Simple generic click listener
 */

public interface ItemClickListener<T> {

    void onItemClick(T item);

}
