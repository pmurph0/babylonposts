package com.pmurphy.babylonposts.util;

import io.reactivex.Scheduler;

/**
 * Abstracts the providing of schedulers
 */

public interface SchedulerProvider {

    Scheduler main();

    Scheduler io();

    Scheduler computation();

}
