package com.pmurphy.babylonposts.data.provider;

import com.pmurphy.babylonposts.data.api.ApiDataService;
import com.pmurphy.babylonposts.data.model.Comment;
import com.pmurphy.babylonposts.data.model.Post;
import com.pmurphy.babylonposts.data.model.User;
import com.pmurphy.babylonposts.data.source.DataSource;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Data source implementation; uses API service
 *
 * Created by PeterMurphy on 20/05/2017.
 */

public class DataProvider implements DataSource {

    private final ApiDataService apiDataService;

    public DataProvider(ApiDataService apiDataService) {
        this.apiDataService = apiDataService;
    }

    @Override
    public Observable<List<Post>> getPosts() {
        return apiDataService.getPosts()
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<List<Comment>> getComments() {
        return apiDataService.getComments()
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<List<User>> getUsers() {
        return apiDataService.getUsers()
                .subscribeOn(Schedulers.io());
    }

}
