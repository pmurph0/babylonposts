package com.pmurphy.babylonposts.data.model;

/**
 * Created by PeterMurphy on 19/05/2017.
 */

public class User {

    private Long id;
    private String name;
    private String email;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
}
