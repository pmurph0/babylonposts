package com.pmurphy.babylonposts.data.source;

import com.pmurphy.babylonposts.data.model.Comment;
import com.pmurphy.babylonposts.data.model.Post;
import com.pmurphy.babylonposts.data.model.User;

import java.util.List;

import io.reactivex.Observable;

/**
 * Abstracted data layer
 *
 * Created by PeterMurphy on 20/05/2017.
 */
public interface DataSource {

    Observable<List<Post>> getPosts();

    Observable<List<Comment>> getComments();

    Observable<List<User>> getUsers();

}
