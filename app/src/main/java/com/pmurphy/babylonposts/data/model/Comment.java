package com.pmurphy.babylonposts.data.model;

/**
 * Created by PeterMurphy on 19/05/2017.
 */

public class Comment {

    private Long id;
    private Long postId;

    public Long getId() {
        return id;
    }

    public Long getPostId() {
        return postId;
    }

}
