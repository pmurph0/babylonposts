package com.pmurphy.babylonposts.data.api;

import com.pmurphy.babylonposts.data.model.Comment;
import com.pmurphy.babylonposts.data.model.Post;
import com.pmurphy.babylonposts.data.model.User;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Retrofit data service class
 *
 * Created by PeterMurphy on 20/05/2017.
 */

public interface ApiDataService {

    @GET("/posts")
    Observable<List<Post>> getPosts();

    @GET("comments")
    Observable<List<Comment>> getComments();

    @GET("users")
    Observable<List<User>> getUsers();

}
