package com.pmurphy.babylonposts;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pmurphy.babylonposts.data.model.Comment;
import com.pmurphy.babylonposts.data.model.Post;
import com.pmurphy.babylonposts.data.model.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 *  Provides mock data pulled from raw json files
 */
public class MockData {

    public List<Post> getPosts() throws FileNotFoundException {
        Type listType = new TypeToken<ArrayList<Post>>(){}.getType();
        File postsFile = new File(getClass().getResource("/posts.json").getFile());

        return new Gson().fromJson(new FileReader(postsFile), listType);
    }

    public List<Comment> getComments() throws FileNotFoundException {
        Type listType = new TypeToken<ArrayList<Comment>>(){}.getType();
        File commentsFile = new File(getClass().getResource("/comments.json").getFile());

        return new Gson().fromJson(new FileReader(commentsFile), listType);
    }

    public List<User> getUsers() throws FileNotFoundException {
        Type listType = new TypeToken<ArrayList<User>>(){}.getType();
        File usersFile = new File(getClass().getResource("/users.json").getFile());

        return new Gson().fromJson(new FileReader(usersFile), listType);
    }
}
