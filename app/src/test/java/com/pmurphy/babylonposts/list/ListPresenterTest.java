package com.pmurphy.babylonposts.list;

import com.pmurphy.babylonposts.MockData;
import com.pmurphy.babylonposts.data.model.Post;
import com.pmurphy.babylonposts.data.source.DataSource;
import com.pmurphy.babylonposts.util.TestSchedulersProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.FileNotFoundException;
import java.util.List;

import io.reactivex.Observable;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ListPresenterTest {

    @Mock
    ListContract.View view;

    @Mock
    DataSource dataSource;

    ListPresenter presenter;

    MockData mockData;
    List<Post> posts;

    @Before
    public void setUp() throws FileNotFoundException {
        MockitoAnnotations.initMocks(this);

        mockData = new MockData();
        posts = mockData.getPosts();

        when(dataSource.getPosts()).thenReturn(Observable.just(posts));

        presenter = new ListPresenter(view, dataSource, new TestSchedulersProvider());
    }

    @Test
    public void testLoadPostsIntoView() {
        presenter.loadPosts();
        verify(dataSource).getPosts();

        verify(view).showPosts(posts);  //FIXME - y u fail
    }

    @Test
    public void testOpenPostDetails() {
        Post post = posts.get(3);
        presenter.onPostClick(post);
        verify(view).showPostDetails(post);
    }

}
