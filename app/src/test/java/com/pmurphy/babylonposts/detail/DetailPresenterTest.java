package com.pmurphy.babylonposts.detail;

import com.pmurphy.babylonposts.MockData;
import com.pmurphy.babylonposts.data.model.Post;
import com.pmurphy.babylonposts.data.source.DataSource;
import com.pmurphy.babylonposts.util.TestSchedulersProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.FileNotFoundException;

import io.reactivex.Observable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DetailPresenterTest {

    @Mock
    DetailContract.View view;

    @Mock
    DataSource dataSource;

    DetailPresenter presenter;
    Post post;

    @Before
    public void setUp() throws FileNotFoundException {
        MockitoAnnotations.initMocks(this);

        MockData mockDataSource = new MockData();
        when(dataSource.getPosts()).thenReturn(Observable.just(mockDataSource.getPosts()));
        when(dataSource.getUsers()).thenReturn(Observable.just(mockDataSource.getUsers()));
        when(dataSource.getComments()).thenReturn(Observable.just(mockDataSource.getComments()));
        post = mockDataSource.getPosts().get(3);

        presenter = new DetailPresenter(dataSource, view, post, new TestSchedulersProvider());
    }

    @Test
    public void testLoadPostDetails() {
        presenter.loadPostDetails();
        verify(dataSource).getUsers();
        verify(dataSource).getComments();

        //FIXME - y u fail tho
        verify(view).showPostTitle(post.getTitle());
        verify(view).showPostBody(post.getBody());
    }

}
