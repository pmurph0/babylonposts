package com.pmurphy.babylonposts.util;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class AvatarUtilsTest {

    @Test
    public void testBuildAdorableAvatarUrl() {
        int size = 500;
        String email = "pnmurphy0@gmail.com";
        Assert.assertThat(AvatarUtils.buildAdorableAvatarUrl(email, size),
                Matchers.is("https://api.adorable.io/avatars/500/pnmurphy0@gmail.com.png"));
    }

}
