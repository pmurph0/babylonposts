package com.pmurphy.babylonposts.util;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.TestScheduler;

/**
 * Provides schedulers for testing to avoid threading issues
 */

public class TestSchedulersProvider implements SchedulerProvider {

    private final Scheduler testScheduler;

    public TestSchedulersProvider() {
        testScheduler = new TestScheduler();
    }

    @Override
    public Scheduler main() {
        return testScheduler;
    }

    @Override
    public Scheduler io() {
        return testScheduler;
    }

    @Override
    public Scheduler computation() {
        return testScheduler;
    }
}
